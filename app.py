import requests
from flask import Flask, jsonify, request





app = Flask(__name__)

@app.route("/")
def home():
    return 'First microservice'

@app.route("/api/v1/getTextFromSecond", methods=['GET'])
def request_second():
    resp = requests.get("http://0.0.0.0:5001/api/v1/getText")
    return resp.json()
    

if __name__ == '__main__':
    app.debug = True
    app.run('0.0.0.0', port=5000)